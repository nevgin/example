provider "gitlab" {
	token = "${var.gitlab_token}"
}

resource "gitlab_project" "example" {
  name        = "example"
  description = "My awesome codebase"
  visibility_level = "public"
}

resource "gitlab_deploy_key" "deploy_key" {
  project =	"${gitlab_project.example.id}" 
  title   = "Example deploy key"
  key     = "${file("~/.ssh/id_rsa.pub")}"
  can_push = "True"
}
